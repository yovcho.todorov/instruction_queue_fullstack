package com.travisperkins.queues;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

public class InstructionMessageTest {

    private InstructionMessage instructionMessage;

    @BeforeEach
    public void setup() {
        instructionMessage = new InstructionMessage();
    }

    @Test
    public void testGetType() {
        instructionMessage.type = "A";
        assertEquals("A", instructionMessage.getType());
    }
    @Test
    public void testReceiveValidInstructionMessage() {
        String validMessage = "A AB14 10 100 2023-05-17T10:30:00.000Z";
        assertDoesNotThrow(() -> instructionMessage.receive(validMessage));
    }
    @Test
    public void testReceiveInvalidMessageType() {
        String invalidMessage = "E AQ45 10 100 2023-05-17T10:30:00.000Z";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> instructionMessage.receive(invalidMessage));
        assertEquals("message type is not valid.", exception.getMessage());
    }
    @Test
    public void testReceiveInvalidProductCode() {
        String invalidMessage = "A ABCDE 10 100 2023-05-17T10:30:00.000Z";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> instructionMessage.receive(invalidMessage));
        assertEquals("product code is not valid.", exception.getMessage());
    }
    @Test
    public void testReceiveInvalidQuantity() {
        String invalidMessage = "A AB55 -1 100 2023-05-17T10:30:00.000Z";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> instructionMessage.receive(invalidMessage));
        assertEquals("quantity is not valid.", exception.getMessage());
    }
    @Test
    public void testReceiveInvalidUOM() {
        String invalidMessage = "A DE78 10 300 2023-05-18T10:30:00.000Z";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> instructionMessage.receive(invalidMessage));
        assertEquals("uom is not valid.", exception.getMessage());
    }
    @Test
    public void testReceiveInvalidTimestampPast() {
        String invalidMessage = "A AK47 10 100 1969-12-31T23:59:59.999Z";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> instructionMessage.receive(invalidMessage));
        assertEquals("timestamp format is not valid.", exception.getMessage());
    }
    @Test
    public void testReceiveInvalidTimestampFuture() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        String futureTimestamp = formatter.format(new Date(System.currentTimeMillis() + 24 * 60 * 60 * 1000)); // Tomorrow's date
        String invalidMessage = "A AB12 10 100 " + futureTimestamp;
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> instructionMessage.receive(invalidMessage));
        assertEquals("timestamp format is not valid.", exception.getMessage());
    }
    @Test
    public void testReceiveInvalidTimestamp() {
        String invalidMessage = "A AB12 10 100 2023-05-17T10:350:00.0Z";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> instructionMessage.receive(invalidMessage));
        assertEquals("timestamp format is not valid.", exception.getMessage());
    }
}
