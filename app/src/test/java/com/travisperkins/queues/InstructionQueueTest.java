package com.travisperkins.queues;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InstructionQueueTest {
    private InstructionQueue instructionQueue;

    @BeforeEach
    void setUp() {
        instructionQueue = new InstructionQueue();
    }
    @Test
    void enqueue_AddsMessageToQueue() {
        InstructionMessage message = new InstructionMessage("A", "AB22", 10, 155, "2023-05-17T10:00:00.000Z");
        instructionQueue.enqueue(message);

        assertEquals(1, instructionQueue.count());
    }
    @Test
    void dequeue_RemovesAndReturnsHighestPriorityMessage() {
        InstructionMessage highPriorityMessage = new InstructionMessage("A", "AC69", 10, 169, "2023-05-17T10:00:00.000Z");
        instructionQueue.enqueue(highPriorityMessage);
        InstructionMessage lowPriorityMessage = new InstructionMessage("C", "DE25", 5, 189, "2023-05-17T10:00:00.000Z");
        instructionQueue.enqueue(lowPriorityMessage);

        InstructionMessage dequeuedMessage = instructionQueue.dequeue();

        assertEquals(highPriorityMessage, dequeuedMessage);
        assertEquals(1, instructionQueue.count());
    }
    @Test
    void peek_ReturnsHighestPriorityMessageWithoutRemovingIt() {
        InstructionMessage highPriorityMessage = new InstructionMessage("A", "AB75", 10, 100, "2023-05-17T10:00:00.000Z");
        instructionQueue.enqueue(highPriorityMessage);
        InstructionMessage lowPriorityMessage = new InstructionMessage("D", "DF77", 5, 200, "2023-05-17T10:00:00.000Z");
        instructionQueue.enqueue(lowPriorityMessage);

        InstructionMessage peekedMessage = instructionQueue.peek();

        assertEquals(highPriorityMessage, peekedMessage);
        assertEquals(2, instructionQueue.count());
    }
    @Test
    void count_ReturnsNumberOfMessagesInQueue() {
        InstructionMessage message1 = new InstructionMessage("A", "AC99", 10, 120, "2023-05-17T10:00:00.000Z");
        instructionQueue.enqueue(message1);
        InstructionMessage message2 = new InstructionMessage("B", "EF44", 5, 222, "2023-05-17T10:00:00.000Z");
        instructionQueue.enqueue(message2);
        InstructionMessage message3 = new InstructionMessage("C", "GH33", 3, 255, "2023-05-17T10:00:00.000Z");
        instructionQueue.enqueue(message3);

        int queueSize = instructionQueue.count();

        assertEquals(3, queueSize);
    }
    @Test
    void isEmpty_ReturnsTrueWhenQueueIsEmpty() {
        boolean isEmpty = instructionQueue.isEmpty();

        assertTrue(isEmpty);
    }
    @Test
    void isEmpty_ReturnsFalseWhenQueueIsNotEmpty() {
        InstructionMessage message = new InstructionMessage("A", "AK47", 10, 1, "2023-05-17T10:00:00.000Z");
        instructionQueue.enqueue(message);

        boolean isEmpty = instructionQueue.isEmpty();

        assertFalse(isEmpty);
    }
}

